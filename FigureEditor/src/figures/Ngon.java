/**
 *
 */
package figures;

import java.util.logging.Logger;

import figures.enums.LineType;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import utils.ColorFactory;

/**
 * Circle Figure containing a {@link javafx.scene.shape.Circle} as its
 * {@link Figure#shape}
 * @warning Since This class is also named "Circle", you'll need to use
 * (javafx.scene.shape.Circle) each time you need to acces to internal
 * {@link Figure#shape} casted as a {@link javafx.scene.shape.Circle}
 * @implSpec It is assumed that {@link Figure#shape} will always be non null
 * during the life cycle of a Circle.
 * @author davidroussel
 */
public class Ngon extends Figure
{
	/**
	 * Instances counter (to be used in {@link Figure#instanceNumber}) of each
	 * Circle.
	 * @implNote No need to decrease {@link Figure#instanceNumber} in
	 * {@link #finalize()}
	 */
	private static int counter = 0;
	
	private double radius;
	private Point2D center;
	private int nbCotes=10;

	/**
	 * Valued constructor to build a zero size Circle at point (x, y).
	 * Used during Cicle construction with {@link MouseEvent}s
	 * Calls super-constructor, sets {@link Figure#instanceNumber} then
	 * {@link #createShape(double, double)} and attach {@link Figure#shape} to
	 * {@link Figure#root}.
	 * @param fillColor the fill color (or null if there is no fill color).
	 * The fill color set in this circle shall be set from {@link ColorFactory}.
	 * @param edgeColor the edge color (or null if there is no edge color)
	 * The edge color set in this circle shall be set from {@link ColorFactory}.
	 * @param lineType line type (Either {@link LineType#SOLID},
	 * {@link LineType#DASHED} or {@link LineType#NONE}). If there is no edge
	 * color provided the internal {@link #lineType} shall be set to
	 * {@link LineType#NONE}
	 * @param lineWidth line width of this circle. If there is no edge
	 * color provided the internal {@link #lineType} shall be set to 0
	 * @param parentLogger a parent logger used to initialize the current logger
	 * @param x the initial x coordinate in the drawing panel where to create this circle
	 * @param y the initial y coordinate in the drawing panel where to create this circle
	 * @throws IllegalStateException if we try to set both fillColor and
	 * edgecolor as nulls
	 */
	public Ngon(Color fillColor,
	              Color edgeColor,
	              LineType lineType,
	              double lineWidth,
	              Logger parentLogger,
	              double x,
	              double y)
	    throws IllegalStateException
	{
		super(fillColor, edgeColor, lineType, lineWidth, parentLogger);
		instanceNumber = counter++;
		createShape(x, y);
		center = new Point2D(x, y);
		root.getChildren().add(shape);
	}

	/**
	 * Valued constructor to build a Circle at point (x, y) with specified radius
	 * Calls super-constructor, sets {@link Figure#instanceNumber} then
	 * {@link #createShape(double, double)} and attach {@link Figure#shape} to
	 * {@link Figure#root}.
	 * @param fillColor the fill color (or null if there is no fill color).
	 * The fill color set in this circle shall be set from {@link ColorFactory}.
	 * @param edgeColor the edge color (or null if there is no edge color)
	 * The edge color set in this circle shall be set from {@link ColorFactory}.
	 * @param lineType line type (Either {@link LineType#SOLID},
	 * {@link LineType#DASHED} or {@link LineType#NONE}). If there is no edge
	 * color provided the internal {@link #lineType} shall be set to
	 * {@link LineType#NONE}
	 * @param lineWidth line width of this circle. If there is no edge
	 * color provided the isetLastPointnternal {@link #lineType} shall be set to 0
	 * @param parentLogger a parent logger used to initialize the current logger
	 * @param x the initial x coordinate in the drawing panel where to create this circle
	 * @param y the initial y coordinate in the drawing panel where to create this circle
	 * @param radius the initial radius of the circle
	 * @throws IllegalStateException if we try to set both fillColor and
	 * edgecolor as nulls
	 */
	public Ngon(Color fillColor,
	              Color edgeColor,
	              LineType lineType,
	              double lineWidth,
	              Logger parentLogger,
	              double x,
	              double y,
	              double width, 
	              double height)
	    throws IllegalStateException
	{
		this(fillColor, edgeColor, lineType, lineWidth, parentLogger, x, y);
		javafx.scene.shape.Rectangle rectangle = (javafx.scene.shape.Rectangle) shape;
		rectangle.setWidth(Math.abs(width));
		rectangle.setHeight(Math.abs(height));
	}

	/**
	 * Copy constructor
	 * @param figure the figure to be copied
	 * @throws IllegalArgumentException if the provided figure is not a Circle
	 */
	public Ngon(Figure figure)
	{
		super(figure);
		if(figure instanceof Ngon) {
			radius= ((Ngon)figure).radius;
			center = ((Ngon)figure).center;
			nbCotes= ((Ngon)figure).nbCotes;
		}
		
		if (!(figure instanceof Ngon))
		{
			String message = "provided figure is not a Rectangle: "
			    + figure.getClass().getSimpleName();
			logger.severe(message);
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Convenience method to get internal {@link Figure#shape} casted as a
	 * {@link javafx.scene.shape.Circle}
	 * @return the internal {@link Figure#shape} casted as a
	 * {@link javafx.scene.shape.Circle}
	 */
	protected javafx.scene.shape.Polygon getNgonShape()
	{
		return (javafx.scene.shape.Polygon) shape;
	}

	/**
	 * Center Point of this figure
	 * @return the center point of this figure
	 */
	@Override
	public Point2D getCenter()
	{
		return this.center;
	}
	
	public double getX()
	{
		return center.getX();
	}
	
	public double getY()
	{
		return center.getY();
	}	
	
	public Double[] getPoints(int n, double r){
		
		Double[] list = new Double[n*2];
		for(int i=0; i<2*n; i+=2) {
			list[i] = center.getX() + r*(Math.cos(2*Math.PI*(i/2)/n));
			list[i+1] = center.getY() + r*(Math.sin(2*Math.PI*(i/2)/n));
		}
		return list;
	}
	
	public double mostLeft()
	{
		boolean xCoord = true;
		double x = 1000000;
		for(double p : getNgonShape().getPoints()) {
			if(xCoord) {
				if(x>p) {
					x = p;
				}
			}
		}
		return x;
	}
	
	public double mostRight()
	{
		boolean xCoord = true;
		double x = -1000000;
		for(double p : getNgonShape().getPoints()) {
			if(xCoord) {
				if(x<p) {
					x = p;
				}
			}
		}
		return x;
	}

	public double mostTop()
	{
		boolean yCoord = false;
		double y = -1000000;
		for(double p : getNgonShape().getPoints()) {
			if(yCoord) {
				if(y<p) {
					y = p;
				}
			}
		}
		return y;
	}
	
	public double mostBottom()
	{
		boolean yCoord = false;
		double y = 1000000;
		for(double p : getNgonShape().getPoints()) {
			if(yCoord) {
				if(y>p) {
					y = p;
				}
			}
		}
		return y;
	}

	/**
	 * Height of this figure
	 * @return the width of this figure
	 */
	@Override
	public double height()
	{
		 return mostTop() - mostBottom();
	}
	

	/**
	 * Width of this figure
	 * @return the width of this figure
	 */
	@Override
	public double width()
	{
		return mostRight() - mostLeft();
	}
	

	/**
	 * Top left corner of this figure
	 * @return the top left {@link Point2D} of this figure
	 */
	@Override
	public Point2D topLeft()
	{
		return new Point2D(this.mostLeft(), this.mostTop());
	}

	/**
	 * Bottom right corner of this figure
	 * @return the bottom right {@link Point2D} of this figure
	 */
	@Override
	public Point2D bottomRight()
	{
		return new Point2D(this.mostRight(), this.mostBottom());
	}

	/**
	 * radius accessor of this Circle
	 * @return the radius of this Circle
	 */
	public double getWidth()
	{
		return this.width();
	}
	
	public double getHeight()
	{
		return this.height();
	}

	/**
	 * Creates actual {@link #shape} at specified position and apply
	 * parameters
	 * @param x the x coordinate of the initial point where to create the new shape
	 * @param y the y coordinate of the initial point where to create the new shape
	 * @post a new {@link #shape} has been created with a new
	 * {@link #instanceNumber} with {@link #fillColor}, {@link #edgeColor},
	 * {@link #lineType} & {@link #lineWidth} applied with
	 * {@link #applyParameters(Shape)}
	 */
	@Override
	public void createShape(double x, double y)
	{
		/*
		 * Note: since This class is also named Circle we need to explicitely
		 * use "new javafx.scene.shape.Circle(...)" here
		 */
		shape = new javafx.scene.shape.Polygon(x, y);
		applyParameters(shape);

	}

	public void setNewSides(double radius, int n) {
		getNgonShape().getPoints().removeAll(getNgonShape().getPoints());
		getNgonShape().getPoints().addAll(this.getPoints(n, radius));

	}

	/**
	 * Sets the last point of this figure.
	 * Sets the radius of this Circle based on the distance between center and
	 * the provided point
	 * @param lastPoint the point used to set this Circle's radius
	 */
	@Override
	public void setLastPoint(Point2D lastPoint)
	{
		setNewSides(center.distance(lastPoint), 10);
	}
	
	public void setSides(int updown)
	{
		nbCotes+=updown;
		setNewSides(radius, nbCotes);
	}
	/**
	 * Creates a copy of this circle (with the same name and instance number)
	 * @return A distinct copy of this circle
	 */
	@Override
	public Figure clone()
	{
		return new Ngon(this);
	}
	
	/**
	 * Compare this circle to another figure
	 * @return true if the other figure is also a Circle with the same
	 * position and size (with 1e-6 threhold), false otherwise.
	 * Other parameters, such as {@link Figure#fillColor},
	 * {@link Figure#edgeColor}, {@link Figure#lineType} and
	 * {@link Figure#lineWidth} are checked in {@link Figure#equals(Object)}
	 * @see Figure#equals(Object)
	 */
	@Override
	protected boolean equals(Figure figure)
	{
		if (!(figure instanceof Ngon))
		{
			return false;
		}

		Ngon circle = (Ngon) figure;

		if (Math.abs(getCenter().distance(circle.getCenter())) > 1e-6)
		{
			return false;
		}

		if (Math.abs(getWidth() - circle.getWidth()) > 1e-6)
		{
			return false;
		}
		if (Math.abs(getHeight() - circle.getHeight()) > 1e-6)
		{
			return false;
		}

		return true;
	}
}
