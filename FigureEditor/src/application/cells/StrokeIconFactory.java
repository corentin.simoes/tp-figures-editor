package application.cells;

import java.util.logging.Level;
import java.util.logging.Logger;

import figures.Figure;
import figures.enums.FigureType;
import figures.enums.LineType;
import javafx.scene.image.Image;
import logger.LoggerFactory;
import utils.IconFactory;

/**
 * Icon Sub-Factory dedicated to Figure Types icons.
 * Suitable for {@link FigureCellController}s and
 * {@link FigureTypeCellController}s
 * @author davidroussel
 */
public class StrokeIconFactory
{
	/**
	 * Stroke_Dashed figure icon
	 */
	private static Image dashed = IconFactory.getIcon("Stroke_Dashed");

	/**
	 * Stroke_None figure icon
	 */
	private static Image none = IconFactory.getIcon("Stroke_None");

	/**
	 * Stroke_Solid figure icon
	 */
	private static Image solid = IconFactory.getIcon("Stroke_Solid");

	/**
	 * Logger to use
	 */
	private static Logger logger = LoggerFactory.getParentLogger(FigureIconsFactory.class,
	                                                             IconFactory.getLogger(),
	                                                             (IconFactory.getLogger() == null ?
	                                                              Level.INFO : null)); // null level to inherits parent logger's level

	/**
	 * Retrieve icon from Figure instance
	 * @param figure the figure to retrieve icon for
	 * @return the corresponding icon image
	 * @throws NullPointerException if provided figure is null and {@link FigureType}
	 * can't be determined
	 */
	public static Image getIconFromInstance(Figure figure)
	{
		if (figure == null)
		{
			String message = "null instance";
			logger.severe(message);
			throw new NullPointerException(message);
		}

		return getIconFromType(figure.getLineType());
	}

	/**
	 * Retrieve icon image from figure Type
	 * @param lineType the type of figure
	 * @return the corresponding icon image
	 * @throws IllegalArgumentException if type is out of {@link FigureType} enum values
	 */
	public static Image getIconFromType(LineType lineType)
	{
		switch (lineType)
		{
			case DASHED:
			{
				return dashed;
			}
			case NONE:
			{
				return none;
			}
			case SOLID:
			{
				return solid;
			}
			
			default:
				String message = "Unexpected value: " + lineType.toString();
				logger.severe(message);
				throw new IllegalArgumentException(message);
		}
	}
}