package tools;

import java.util.logging.Level;
import java.util.logging.Logger;

import figures.Drawing;
import history.HistoryManager;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Node;
import javafx.scene.input.ScrollEvent;
import logger.LoggerFactory;

/**
 * Base class of all tools intercepting {@link MouseEvent}s either as an Event
 * Filter using {@link Node#addEventFilter(EventType, EventHandler)} to
 * intercept events during capture phase (closer to the source), or as an Event
 * Handler using {@link Node#addEventHandler(EventType, EventHandler)} to
 * intercept events during the bubbling phase (farther from the source,
 * typically in a parent node}
 * @author davidroussel
 * @param <T> The type of root node this Handler should be attached to
 */
public class AbstractToolScroll<T extends Node> implements EventHandler<ScrollEvent>
{
	/**
	 * The root {@link Node} this Handler should be registered to.
	 * root node can also be used later to {@link #unregister()} this tool from it.
	 */
	protected T root;

	/**
	 * The logger to use to issue messages
	 */
	protected Logger logger;

	/**
	 * Boolean flag indicating if this {@link EventHandler} should capture events
	 * during events capture phase (faster) or during events bubbling phase
	 * (slower, but required to capture events bubbling from children nodes).
	 */
	protected boolean captureEvents;

	/**
	 * Boolean flag indicating events should be consumed after being processed.
	 * captured events should typically be consumed, bubbling events don't need
	 * to be consumed (but they also can be consumed)
	 */
	protected boolean consumeEvents;

	/**
	 * The bitmask of events to listen to.
	 * A New bit mask can be composed with bitwise ORs : e.g.
	 * <code>(PRESSED|RELEASED)</code>.
	 * Checking for specific events can be performed with listenXXXEvents() methods
	 * @see #listenPressedEvents()
	 * @see #listenReleasedEvents()
	 * @see #listenClickedEvents()
	 * @see #listenMovedEvents()
	 * @see #listenDraggedEvents()
	 */
	protected int bitMask;

	

	/**
	 * Constant to check with {@link #bitMask} in
	 * {@link #AbstractTool(Node, Drawing, Label, int, HistoryManager, Logger)}
	 * to define if {@link #mousePressed(MouseEvent)} should be triggered on
	 * {@link MouseEvent#MOUSE_EXITED} events
	 */
	protected static final int SCROLL = 163;

	/**
	 * Default Constructor.
	 * Initialize all attributes to their default value
	 * Using this constructor will require to use
	 * {@link #setup(Node, int, boolean, boolean, Logger)} later to properly
	 * set up attributes.
	 * @apiNote This constructor is NOT suitable to create a working Tool, since
	 * there is no {@link #root} to listen to, nor events mask to define events.
	 * @apiNote This constructor is however suitable to create an FXML controller
	 * since loading FXML associated with a controller will only trigger the
	 * default constructor of this controller.
	 */
	public AbstractToolScroll()
	{
		root = null;
		logger = null;
		captureEvents = false;
		consumeEvents = false;
		bitMask = 0;
	}

	/**
	 * Setup method to provide values to all attributes.
	 * Register this tool either as an Event Filter with
	 * {@link Node#addEventFilter(EventType, EventHandler)} or as an Event
	 * Handler with {@link Node#addEventHandler(EventType, EventHandler)} on
	 * the {@link #root} node to listen to {@link MouseEvent}s specified in the
	 * eventsMask.
	 * @param root the JavaFX root {@link Node} this handler is attached to
	 * @param eventsMask The bit mask of events we should listen to.
	 * e.g. (PRESSED|RELEASED) will only register #mousePressed and
	 * #mouseReleased
	 * @param capture if true indicate this tool should be registered on the
	 * root as an Event Filter, otherwise it sholud be registered on the root as
	 * an {@link EventHandler}
	 * @param consume infdicates if events should be consumed after being
	 * processed or not.
	 * @param parentLogger parent logger
	 */
	public void setup(T root,
	                  int eventsMask,
	                  boolean capture,
	                  boolean consume,
	                  Logger parentLogger)
	{
		this.root = root;
		bitMask = eventsMask;
		captureEvents = capture;
		consumeEvents = consume;
	}

		
	/**
	 * Valued constructor.
	 * Register this tool either as an Event Filter with
	 * {@link Node#addEventFilter(EventType, EventHandler)} or as an Event
	 * Handler with {@link Node#addEventHandler(EventType, EventHandler)} on
	 * the {@link #root} node to listen to {@link MouseEvent}s specified in the
	 * eventsMask.
	 * @param root the JavaFX root {@link Node} this handler is attached to
	 * @param eventsMask The bit mask of events we should listen to.
	 * e.g. (PRESSED|RELEASED) will only register #mousePressed and
	 * #mouseReleased
	 * @param capture if true indicate this tool should be registered on the
	 * root as an Event Filter, otherwise it sholud be registered on the root as
	 * an {@link EventHandler}
	 * @param consume infdicates if events should be consumed after being
	 * processed or not.
	 * @param parentLogger parent logger
	 */
	public AbstractToolScroll(T root,
	                    int eventsMask,
	                    boolean capture,
	                    boolean consume,
	                    Logger parentLogger)
	{
		super();
		if ((root != null) && (eventsMask != 0) && (parentLogger != null))
		{
			setup(root, eventsMask, capture, consume, parentLogger);
		}
		else
		{
			this.root  = null;
			logger = null;
			captureEvents = capture;
			consumeEvents = consume;
			bitMask = 0;
		}
	}

	/**
	 * Specialized constructor with {@link #captureEvents} and
	 * {@link #consumeEvents} automatically set to true.
	 * Register this tool either as an Event Filter with
	 * {@link Node#addEventFilter(EventType, EventHandler)} or as an Event
	 * Handler with {@link Node#addEventHandler(EventType, EventHandler)} on
	 * the {@link #root} node to listen to {@link MouseEvent}s specified in the
	 * eventsMask.
	 * @param root the JavaFX root {@link Node} this handler is attached to
	 * @param eventsMask The bit mask of events we should listen to.
	 * e.g. (PRESSED|RELEASED) will only register #mousePressed and
	 * #mouseReleased
	 * @param parentLogger parent logger
	 */
	public AbstractToolScroll(T root,
	                    int eventsMask,
	                    Logger parentLogger)
	{
		this(root, eventsMask, true, true, parentLogger);
	}
	/**
	 * Register this tool on the {@link #root} node either as an Event Filter (to
	 * intercept events during capture phase) or as an Event Handler (to
	 * intercept events during bubbling phase) according to
	 * {@link #captureEvents}'s value.
	 * @param eventType the type of events this tool should intercept
	 */

	@Override
	public void handle(ScrollEvent e) {
		e.getDeltaY();
		// TODO Auto-generated method stub
		
	}
}
